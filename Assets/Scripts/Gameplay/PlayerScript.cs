using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the player's movement when away from the laptop.
/// </summary>

public class PlayerScript : MonoBehaviour
{
    private static PlayerScript Instance;
    public bool canMove = false;

    private float moveSpeed = 15.0f;

    private float mouseSensitivity = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            canMove = true;
        }

        if (canMove)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.position += transform.forward * moveSpeed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.A))
            {
                transform.position -= transform.right * moveSpeed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.S))
            {
                transform.position -= transform.forward * moveSpeed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.D))
            {
                transform.position += transform.right * moveSpeed * Time.deltaTime;
            }

            float nextHorizontalPos = transform.eulerAngles.y + mouseSensitivity * Input.GetAxis("Mouse X");
            float nextVerticalPos = transform.eulerAngles.x + mouseSensitivity * -Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(nextVerticalPos, nextHorizontalPos, transform.eulerAngles.z);
        }
    }
}
